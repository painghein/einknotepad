/*
 * Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gajah.einknotepad;

import android.util.Log;

public class LogUtil {
	public String TAG = "LogUtil";
	/**
	 * output message if true.
	 * <p>
	 * otherwise nothing to do
	 */
	public static boolean isOutput = true;

	public static void e(String tag, String msg) {
		if (isOutput)
			Log.e(tag, msg);
	}

	public static void i(String tag, String msg) {
		if (isOutput)
			Log.i(tag, msg);
	}

	public static void d(String tag, String msg) {
		if (isOutput)
			Log.d(tag, msg);
	}

	public static final String DefaultTag = "LogUtil";

	public static void E(String msg) {
		if (isOutput)
			Log.e(DefaultTag, msg);
	}

	public static void I(String msg) {
		if (isOutput)
			Log.i(DefaultTag, msg);
	}

	public static void D(String msg) {
		if (isOutput)
			Log.d(DefaultTag, msg);
	}

}
