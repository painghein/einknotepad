/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * 
 *  * Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gajah.einknotepad;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;

import com.gajah.einknotepad.NotePad.Notes;
import com.gajah.einksdk.EinkKeyEvent;

/**
 * Displays a list of notes. Will display notes from the {@link Uri} provided in
 * the intent if there is one, otherwise defaults to displaying the contents of
 * the {@link NotePadProvider}
 */
public class NotesList extends FragmentActivity implements OnItemClickListener,
		LoaderManager.LoaderCallbacks<Cursor> {
	private static final String TAG = "NotesList";

	// Menu item ids
	public static final int MENU_ITEM_DELETE = Menu.FIRST;
	public static final int MENU_ITEM_INSERT = Menu.FIRST + 1;

	public static final int VIEW_COUNT = 10;

	/**
	 * The columns we are interested in from the database
	 */
	private static final String[] PROJECTION = new String[] { Notes._ID, // 0
			Notes.TITLE, // 1
	};

	ListView list;
	TextView mPagenum;
	ArrayList<HashMap<String, Object>> mdata;
	private AdapterPageList listAdapter;
	private final int MSG_UPDATE_PAGE_NUM = 1;
	MenuPopup demopw;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case MSG_UPDATE_PAGE_NUM:
				mPagenum.setText((CharSequence) msg.obj);
				break;
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setDefaultKeyMode(DEFAULT_KEYS_SHORTCUT);

		// If no data was given in the intent (because we were started
		// as a MAIN activity), then use our default content provider.
		Intent intent = getIntent();
		if (intent.getData() == null) {
			intent.setData(Notes.CONTENT_URI);
		}

		setContentView(R.layout.notes_display);
		list = (ListView) findViewById(R.id.list);
		mPagenum = (TextView) findViewById(R.id.tvPagenum);

		// Used to map notes entries from the database to views
		mdata = new ArrayList<HashMap<String, Object>>();
		list.setOnItemClickListener(this);
		list.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				int index = listAdapter.getIndex();
				LogUtil.I("----------Activity::onKey-----------Key Code"
						+ event.getKeyCode());
				if ((keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == EinkKeyEvent.KEYCODE_PAGEDOWN)
						&& event.getAction() == KeyEvent.ACTION_DOWN) {
					if (index-- > 1) {
						LogUtil.I("----------mListview::onKey-----------pre"
								+ index);
						listAdapter.setIndex(index);
						listAdapter.notifyDataSetChanged();

						Object o = index
								+ "/"
								+ (listAdapter.getTotalpage() > 0 ? listAdapter
										.getTotalpage() : 1);
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				} else if ((keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == EinkKeyEvent.KEYCODE_PAGEUP)
						&& event.getAction() == KeyEvent.ACTION_DOWN) {

					if ((float) index++ < (float) mdata.size()
							/ listAdapter.VIEW_COUNT) {
						LogUtil.I("----------mListview::onKey-----------next"
								+ index);
						listAdapter.setIndex(index);
						listAdapter.notifyDataSetChanged();

						Object o = index
								+ "/"
								+ (listAdapter.getTotalpage() > 0 ? listAdapter
										.getTotalpage() : 1);
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				}
				return false;
			}
		});
		registerForContextMenu(list);

		getSupportLoaderManager().initLoader(0, null, this);
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v == list) {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
			int index = ((((AdapterPageList) list.getAdapter()).getIndex() - 1) * VIEW_COUNT)
					+ info.position;

			LogUtil.D("" + index);
			list.setSelection(index);
			if (demopw == null) {
				demopw = new MenuPopup(this);
				demopw.show(this.findViewById(R.id.test_demo), 3, 3, index);
			} else {
				demopw.show(this.findViewById(R.id.test_demo), 3, 3, index);
			}
		}
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {

		if (demopw != null && demopw.isShowing()) {
			return demopw.dispatchKeyEvent(event);
		} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			int actual_position = -1;
			if (list.getSelectedItemPosition() > -1) {
				actual_position = ((((AdapterPageList) list.getAdapter())
						.getIndex() - 1) * VIEW_COUNT)
						+ list.getSelectedItemPosition();
			}
			if (demopw == null) {
				demopw = new MenuPopup(this);
				demopw.show(this.findViewById(R.id.test_demo), 3, 3,
						actual_position);
			} else {
				demopw.show(this.findViewById(R.id.test_demo), 3, 3,
						actual_position);
			}
			demopw.demopopup.setOnDismissListener(new OnDismissListener() {
				public void onDismiss() {
					getSupportLoaderManager().getLoader(0).takeContentChanged();
				}

			});
		}
		return super.dispatchKeyEvent(event);
	}

	public void sendMessage(int msgCode, Object o) {
		Message m = new Message();
		m.what = msgCode;
		m.obj = o;
		mHandler.sendMessage(m);
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		int actual_position = ((((AdapterPageList) list.getAdapter())
				.getIndex() - 1) * VIEW_COUNT) + position;
		LogUtil.e(TAG + "Clicking Header", "position is " + actual_position
				+ "  Id is " + id);
		Uri uri = ContentUris.withAppendedId(
				getIntent().getData(),
				Long.parseLong(mdata.get(actual_position).get(Notes._ID)
						.toString()));

		String action = getIntent().getAction();
		if (Intent.ACTION_PICK.equals(action)
				|| Intent.ACTION_GET_CONTENT.equals(action)) {
			// The caller is waiting for us to return a note selected by
			// the user. The have clicked on one, so return it now.
			setResult(RESULT_OK, new Intent().setData(uri));
		} else {
			// Launch activity to view/edit the currently selected item
			startActivity(new Intent(Intent.ACTION_EDIT, uri));
		}

	}

	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(NotesList.this, getIntent().getData(),
				PROJECTION, null, null, Notes.DEFAULT_SORT_ORDER);
	}

	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		// Swap the new cursor in. (The framework will take care of closing the
		// old cursor once we return.)
		HashMap<String, Object> item;
		mdata.clear();
		if (data != null && data.getCount() > 0) {
			data.moveToFirst();
			do {
				item = new HashMap<String, Object>();
				int titleColumn = data.getColumnIndex(Notes.TITLE);
				int IDColumn = data.getColumnIndex(Notes._ID);
				item.put(Notes.TITLE, data.getString(titleColumn));
				item.put(Notes._ID, data.getString(IDColumn));
				mdata.add(item);
			} while (data.moveToNext());

		}
		listAdapter = new AdapterPageList(NotesList.this, mdata,
				R.layout.noteslist_item, new String[] { Notes.TITLE },
				new int[] { android.R.id.text1 }, VIEW_COUNT);
		list.setAdapter(listAdapter);
		Object o = listAdapter.getIndex()
				+ "/"
				+ +(listAdapter.getTotalpage() > 0 ? listAdapter.getTotalpage()
						: 1);
		sendMessage(MSG_UPDATE_PAGE_NUM, o);
	}

	public void onLoaderReset(Loader<Cursor> loader) {
		// This is called when the last Cursor provided to onLoadFinished()
		// above is about to be closed. We need to make sure we are no
		// longer using it.
		list.setAdapter(null);
	}

	public class MenuPopup implements OnKeyListener {
		public PopupWindow demopopup;
		private ListView mListview;
		private View inflateView;

		private final int ADD_NOTE = 0;
		private final int EDIT_NOTE = 1;
		private final int EDIT_TITLE = 2;
		private final int DELETE_NOTE = 3;

		private int selected_index = -1;

		public MenuPopup(Activity context) {
			LayoutInflater inflate = LayoutInflater.from(context);
			inflateView = inflate.inflate(R.layout.menu_notes_list, null);
			mListview = (ListView) inflateView
					.findViewById(R.id.menu_popupwindow_listview);
			demopopup = new PopupWindow(inflateView, 300,
					LayoutParams.WRAP_CONTENT);
			demopopup.setFocusable(true);
		}

		public void initData() {
			String[] items;
			if (selected_index > -1) {
				items = getResources().getStringArray(R.array.notes_list_menu);
			} else {
				items = getResources()
						.getStringArray(R.array.notes_list_menu_1);
			}
			ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
					NotesList.this, R.layout.noteslist_item, items);
			mListview.setAdapter(listAdapter);
			mListview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			mListview.clearChoices();
			mListview.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					LogUtil.D("menu clicking");
					mListview.setItemChecked(position, true);
					mListview
							.dispatchKeyEvent(new KeyEvent(
									KeyEvent.ACTION_DOWN,
									KeyEvent.KEYCODE_DPAD_CENTER));
				}

			});
			mListview.setOnKeyListener(this);
		}

		public void show(View parent, int x, int y, int index) {
			selected_index = index;
			initData();
			LogUtil.e("Menu Pop Up", index + "");
			demopopup.showAtLocation(parent, Gravity.NO_GRAVITY, x, y);

		}

		public boolean isShowing() {
			if (demopopup != null)
				return demopopup.isShowing();
			return false;
		}

		public void dismiss() {
			if (demopopup != null)
				demopopup.dismiss();
			// demopopup=null;
		}

		public boolean dispatchKeyEvent(KeyEvent event) {
			// TODO Auto-generated method stub
			LogUtil.I("----------mListview::dispatchKeyEvent-----------"
					+ event.getKeyCode());

			if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				dismiss();
				return true;
			} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				dismiss();
				return true;
			} else if ((event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER)
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				int index = mListview.getCheckedItemPosition();
				if (index < 0) {
					index = mListview.getSelectedItemPosition();
				}

				LogUtil.E("Selected position is " + index);
				switch (index) {
				case ADD_NOTE:
					dismiss();
					// Launch activity to insert a new item
					startActivity(new Intent(Intent.ACTION_INSERT, getIntent()
							.getData()));
					break;
				case EDIT_NOTE:
					dismiss();
					list.performItemClick(list.getSelectedView(),
							list.getSelectedItemPosition(),
							list.getSelectedItemId());
					break;
				case EDIT_TITLE:
					dismiss();
					Uri noteUri1 = ContentUris.withAppendedId(getIntent()
							.getData(), Long.parseLong(mdata
							.get(selected_index).get(Notes._ID).toString()));
					startActivity(new Intent(TitleEditor.EDIT_TITLE_ACTION,
							noteUri1));
					break;
				case DELETE_NOTE:
					dismiss();
					Uri noteUri = ContentUris.withAppendedId(getIntent()
							.getData(), Long.parseLong(mdata
							.get(selected_index).get(Notes._ID).toString()));
					getContentResolver().delete(noteUri, null, null);
					break;
				}
				return true;
			}

			return false;
		}

		public boolean onKey(View v, int keyCode, KeyEvent event) {
			return dispatchKeyEvent(event);
		}

	}

}
